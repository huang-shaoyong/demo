package com.woniu.logger.sql;


/**
 * 功能描述: 数据库语句 <br/>
 */
public interface IDdlSql {

    /**
     * 查询表是否存在
     */
    String queryTable(String date);

    /**
     * 建表语句
     */
    String createTable();

    /**
     * 备份表
     */
    String backTable(String date);

    /**
     * 删除表
     */
    String dropTable();

}
