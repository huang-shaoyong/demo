package xyz.hlh.crypto.common.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * @author wn
 * @description 统一的返回结果
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class Result<T> {

    @Builder.Default
    private int status = HttpStatus.OK.value();
    private T data;
    @Builder.Default
    private String message = "成功";

}