package com.woniu.service;

/**
 * @className: OrderService
 * @author: woniu
 * @date: 2023/3/25
 **/
public interface OrderService {

    public OrderResVO saveOrder(OrderReqVO reqVO);
}
