package com.woniu.mp.mapstruct.first;

import lombok.Data;

@Data
public class Source {
 private String stringProp;
 private Long longProp;
}

