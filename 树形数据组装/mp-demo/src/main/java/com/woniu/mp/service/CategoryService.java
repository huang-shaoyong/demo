package com.woniu.mp.service;


import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.mp.entity.CategoryEntity;

import java.util.List;

public interface CategoryService extends IService<CategoryEntity> {



    List<Tree<Long>> listWithTree();


}

