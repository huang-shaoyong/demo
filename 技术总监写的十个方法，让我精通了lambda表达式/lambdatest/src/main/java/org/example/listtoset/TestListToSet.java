package org.example.listtoset;

import com.google.common.collect.Lists;
import org.example.entity.OrderItem;
import org.junit.Test;

import java.util.List;
import java.util.Set;
//List、Set 类型之间的转换
public class TestListToSet {
    @Test
    public void testMapToList2() {
        List<OrderItem> orderItems = Lists.newArrayList(
                new OrderItem(1, 5d, "手表"),
                new OrderItem(2, 6d, "机器人"),
                new OrderItem(3, 8d, "手机")
        );
        List<Integer> orderIds = ListToSet.mapToList(orderItems, (item) -> item.getOrderId());
    }

    @Test
    public void testMapToSetV2() {
        List<OrderItem> orderItems = Lists.newArrayList(
                new OrderItem(1, 5d, "手表"),
                new OrderItem(2, 6d, "机器人"),
                new OrderItem(3, 8d, "手机")
        );
        Set<Integer> orderIds = ListToSet.mapToSet(orderItems, (item) -> item.getOrderId());
    }

}
