package com.woniu.service.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.service.entity.Order;
import com.woniu.service.mapper.OrderMapper;
import com.woniu.service.service.IOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  状态枚举类
 * </p>
 *
 * @author 公众号：【程序员蜗牛g】
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

}
